#!/usr/bin/env python3
#-*- coding:utf-8 -*-

class Kinderiano():
    def __init__(self, nombre):
        self._nombre = nombre
        self._lista_vasos = []
    @property
    def nombre(self):
        return self._nombre
    
    def anadir_vasos(self, vasos):
        self._lista_vasos.append(vasos)
    
    def entregavaso(self, vaso):
        return self._lista_vasos[vaso].nombre
    
