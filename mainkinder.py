#!/usr/bin/env python3
#-*- coding:utf-8 -*-
import os 
import random
#from tkinter import N
from plantas import Planta
from kinderiano import Kinderiano
from jardin import Jardin

def main():
    
    
    lista_kinderianos = [Kinderiano("Alicia"),
                         Kinderiano("Andres"),
                         Kinderiano("Belen"),
                         Kinderiano("David"),
                         Kinderiano("Eva"),
                         Kinderiano("jose"),
                         Kinderiano("Marit"),
                         Kinderiano("larry"),
                         Kinderiano("Lucia"),
                         Kinderiano("Pepito"),
                         Kinderiano("Rocio"),
                         Kinderiano("Sergio")]
    
    #lista_plantas = [Hierba, Trebol, Rabanos, Violetas]
    
    jardincito = Jardin(lista_kinderianos)
    jardincito.los_kinderianos()
    jardincito.interaccion_menu()
    
    
if __name__ == "__main__":
    os.system("clear")
    main()

