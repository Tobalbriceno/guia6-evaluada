#!/usr/bin/env python3
#-*- coding:utf-8 -*-
from kinderiano import Kinderiano
import random
from hierba import Hierba
from rabano import Rabano
from violeta import Violeta
from trebol import Trebol

class Jardin():
    def __init__(self, kinderianos):
        self._kinderianos = kinderianos

    #otorgar vasos a los kinderianos
    def los_kinderianos(self):
        for kinderiano in self._kinderianos:
            for vasos in range(4):
                vasitos = random.randrange(3)
                if vasitos == 0:
                    kinderiano.anadir_vasos(Hierba("h"))
                if vasitos == 1:
                    kinderiano.anadir_vasos(Violeta("v"))
                if vasitos == 2:
                    kinderiano.anadir_vasos(Trebol("t"))
                if vasitos == 3:
                    kinderiano.anadir_vasos(Rabano("r"))



            
    def interaccion_menu(self):
        while True:
            print("[ ventana  ][  ventana  ][  ventana ]")
            contador = 0
            for x_kinderiano in self._kinderianos:
                print(x_kinderiano.entregavaso(0),end="")
                print(x_kinderiano.entregavaso(1),end="")
                if contador < 11:
                    print("-",end="")
                contador += 1
            contador = 0   
            print("")
            for x_kinderiano in self._kinderianos:
                print(x_kinderiano.entregavaso(2),end="")
                print(x_kinderiano.entregavaso(3),end="")
                if contador < 11:
                    print("-",end="")
                contador += 1
            print("")
            for i in range(len(self._kinderianos)):
                print(i, " ",self._kinderianos[i].nombre)
            seleccion = input("desea preguntar por las plantas de un kinderiano?\n(S/n)")
            if seleccion == "S":
                seleccionalumnos = int(input("seleccione al kinderiano por su numero de lista"))
                print("Las plantas de ", self._kinderianos[seleccionalumnos].nombre, " son:")
                for i in range(4):
                    if self._kinderianos[seleccionalumnos].entregavaso(i) == "h":
                        print("Hierba")
                    if self._kinderianos[seleccionalumnos].entregavaso(i) == "r":
                        print("Rabano")
                    if self._kinderianos[seleccionalumnos].entregavaso(i) == "t":
                        print("Trebol")
                    if self._kinderianos[seleccionalumnos].entregavaso(i) == "v":
                        print("Violeta")
            if seleccion == "n":
                break
                    
                 